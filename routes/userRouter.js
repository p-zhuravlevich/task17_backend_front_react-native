const express = require("express");
const userController = require("../controllers/userController");
const userRouter = express.Router();
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
const auth = require("../auth");

userRouter.post("/create", urlencodedParser, auth.verifyToken, auth.isAdmin, userController.createUser);
userRouter.delete("/delete/:id", urlencodedParser, auth.verifyToken, auth.isAdmin, userController.deleteUser);
userRouter.put("/update", urlencodedParser, auth.verifyToken, userController.updateUser);
userRouter.get("/getbyid/:id", urlencodedParser, auth.verifyToken, userController.getUserByID);
userRouter.get("/getallusers", urlencodedParser, auth.verifyToken, userController.getAllUsers);

 
module.exports = userRouter;